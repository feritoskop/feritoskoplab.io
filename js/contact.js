$(document).ready(function(){
    
    $('#contact-form').submit(function(event){
        event.preventDefault();

        $("#spinner").show();

        var name = $('#name').val();
        var email = $('#email').val();
        var message = $('#message').val();

        $.ajax({
            method:"POST", 
            url: "contact_handler.php", 
            data:{name:name, email:email, message:message}, 
            success:function(data) { 
                $("form").trigger("reset");
                $("#spinner").hide();
                $('#success_message').fadeIn().html(data);  
                    setTimeout(function(){  
                            $('#success_message').fadeOut("Slow");  
                    }, 2000);
            }
        });
    });

});
