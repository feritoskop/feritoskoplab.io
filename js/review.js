//JS validation 
//ajax POST -> save-review.php
$(document).ready(function(){
    $('#review-form').submit(function (event) {
        event.preventDefault();

        var name = $('#name').val();
        var email = $('#email').val();
        var message = $('#message').val();
        var rating = $('#selected_rating').val();
        if(rating == 0)
        {  
            $('#error_message').fadeIn().html("Please fill out all fields including your rating!");  
            setTimeout(function(){  
                $('#error_message').fadeOut("Slow");  
            }, 2000);
        }
        else  
        {  
            $('#error_message').html('');  
            $.ajax({  
                url:"save-review.php",  
                method:"POST",  
                data:{name:name, email:email, message:message, rating:rating},  
                success:function(data){
                    resetForm();
                    $('#success_message').fadeIn().html(data);  
                    setTimeout(function(){  
                            $('#success_message').fadeOut("Slow");  
                    }, 2000);
                }  
            });  
        }
    });

    $("#clear").click(function(){
        resetForm();
    });
    
    function resetForm(){
        $("form").trigger("reset");

        $("#selected_rating").val(0);
        $(".selected-rating").empty();
        $(".selected-rating").html(0);
        
        for (i = 1; i <= 5; ++i) {
            $("#rating-star-"+i).removeClass('btn-warning');
        }
    };
    
});
